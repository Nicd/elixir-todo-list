# TodoList

Simple TODO list application example for Vincit Functional Programming Day 2018.

## Installation

1. `git clone`
2. `mix deps.get`
3. `iex -S mix`

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc).
