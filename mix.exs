defmodule TodoList.MixProject do
  use Mix.Project

  def project do
    [
      app: :todo_list,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {TodoList.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug, "~> 1.4"},
      {:jason, "~> 1.0"},
      {:cowboy, "~> 1.1"}
    ]
  end
end
