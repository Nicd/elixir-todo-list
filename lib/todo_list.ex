defmodule TodoList do
  @moduledoc """
  Todo list server that holds todo items.
  """

  use GenServer

  # CLIENT

  @spec add_item(pid, String.t()) :: number
  def add_item(server, text) do
    GenServer.call(server, {:add, text})
  end

  @spec del_item(pid, number) :: nil
  def del_item(server, id) do
    GenServer.call(server, {:del, id})
  end

  @spec mark(pid, number, :done | :wip) :: nil
  def mark(server, id, status) do
    GenServer.call(server, {:mark, id, status})
  end

  @spec list(pid) :: %{id: %{text: String.t(), status: atom}}
  def list(server) do
    GenServer.call(server, :list)
  end

  # SERVER

  def start_link(opts) do
    GenServer.start_link(__MODULE__, {0, %{}}, opts)
  end

  def init(state) do
    {:ok, state}
  end

  def handle_call({:add, text}, _from, {counter, items}) do
    counter = counter + 1
    item = %{text: text, status: :wip}
    items = Map.put(items, counter, item)
    {:reply, {counter, item}, {counter, items}}
  end

  def handle_call({:del, id}, _from, {counter, items}) do
    {:reply, :ok, {counter, Map.delete(items, id)}}
  end

  def handle_call({:mark, id, status}, _from, {counter, items}) do
    items = put_in(items, [id, :status], status)
    {:reply, :ok, {counter, items}}
  end

  def handle_call(:list, _from, {counter, items}) do
    {:reply, items, {counter, items}}
  end
end
