defmodule TodoList.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    port =
      case System.get_env("PORT") do
        nil -> 8080
        port -> String.to_integer(port)
      end

    # List all child processes to be supervised
    children = [
      TodoList.child_spec(name: TodoList.Server),
      Plug.Adapters.Cowboy.child_spec(
        :http,
        TodoList.Router,
        [],
        port: port
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TodoList.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
