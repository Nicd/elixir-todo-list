defmodule TodoList.Router do
  @moduledoc """
  Router for our TodoList API.
  """

  use Plug.Router
  use Plug.ErrorHandler
  require Logger

  plug(Plug.Logger)
  plug(:match)

  plug(
    Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason
  )

  plug(:dispatch)

  post "/item" do
    {id, item} = TodoList.add_item(TodoList.Server, conn.body_params["text"])
    send_resp(conn, 200, Jason.encode!(%{id: id, item: item}))
  end

  delete "/item/:id" do
    :ok = TodoList.del_item(TodoList.Server, String.to_integer(id))
    send_resp(conn, 204, "")
  end

  patch "/item/:id" do
    :ok =
      TodoList.mark(
        TodoList.Server,
        String.to_integer(id),
        conn.body_params["status"] |> String.to_existing_atom()
      )

    send_resp(conn, 204, "")
  end

  get "/item" do
    list = TodoList.list(TodoList.Server)
    send_resp(conn, 200, Jason.encode!(list))
  end

  match _ do
    send_resp(conn, 404, Jason.encode!(%{error: "Invalid path."}))
  end

  defp handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stack} = data) do
    Logger.error(inspect(data))
    send_resp(conn, conn.status, "Something went wrong.")
  end
end
