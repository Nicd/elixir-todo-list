defmodule TodoListTest do
  use ExUnit.Case
  doctest TodoList

  test "adds todo" do
    {:ok, pid} = TodoList.start_link([])
    text = "Do it"

    {1, %{status: :wip, text: text}} = TodoList.add_item(pid, text)
    assert TodoList.list(pid) == %{1 => %{status: :wip, text: text}}
  end
end
